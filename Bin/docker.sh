#!/usr/bin/env bash


function printInfo {
printf  "\e[38;5;2m"
printf  "Pallino Docker command\n"
printf  "versione 1.0.0\n"
printf  "Author Pallino & Co. Srl\n"
printf  "===========================\n"
printf  "usage: Bin/docker.sh [start,stop]\n"
printf "\e[0m"
}

params=$#
if [ $# -eq 1 ]; then
	case "$1" in
      "start")
        source develop.sh up -d
        ;;
      "stop")
        source develop.sh down
        ;;
      "remove")
      	source develop.sh remove
      	;;
      *)
      # Should not occur
        printf "\e[38;5;1mError: wrong parameters.\e[0m\n"
		printInfo
		exit 1
        ;;
    esac
else
	printf "\e[31mError: wrong parameters.\e[0m\n"
	printInfo
	exit 1
fi