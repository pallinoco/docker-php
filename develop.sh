#!/usr/bin/env bash

function checkForNetwork {
	docker_network_list=`docker network ls --format "{{.Name}}"`
	FOUND=0
	for i in $docker_network_list
	do
		if [ $i = "devnet" ]; then
			FOUND=1
			continue
		fi
	done
	if [ $FOUND = "0" ]; then
		printf "\n\e[38;5;11mPlease launch network install before this command.\e[0m\e[38;5;2m Use: ./docker_network.sh\n"
		exit 1
	fi
}

function proxyup {
docker_list=`docker ps --format "{{.Names}}"`
FOUND=0
for i in $docker_list
do
	if [ $i = "urlredirect_web_1" ]; then
		FOUND=1
		continue
	fi
done
if [ $FOUND = "0" ]; then
    printf "starting proxy ... "
    actual_path=`pwd`
    if [ -f "docker-proxy.id" ]; then
    	PROXY_PATH=`cat docker-proxy.id`
    	if [ -d $PROXY_PATH ]; then
			cd $PROXY_PATH
			 if [ -f "docker-compose.yml" ]; then
				docker-compose up -d
				if [ $? -eq 0 ]
				then
				  printf "\e[38;5;2mproxy started succesfully\n\n\e[0m"
				else
				  printf "\e[38;5;1mproxy not started\e[0m\n"
				fi
			else
				printf "\e[38;5;1mproxy not started\e[0m\n"
			fi
			cd $actual_path
		else
				printf "\e[38;5;1mproxy not started\e[0m\n"
		fi
	else
		printf "\e[38;5;1mproxy not started\e[0m\n"
	fi
fi
}

function updateSyncFile {
	n=$(grep --exclude=develop.sh -R "src-xxx-sync" * |wc -l)
	if [ $n -gt 0 ]; then
		echo "There are also some files with sync name set to 'src-xxx-sync', so project is not well configured. Please, choose a new name for your docker volume (it replaces src-xxx-sync, suggested src-projectname-sync):"
		read value
		if [ "$value" = "" ]; then
			printf "\e[38;5;1mYou have just typed an empty value, it isn't accepted\e[0m\n"
			exit 1
		fi
		sed -i '' "s/src-xxx-sync/$value/g" docker-sync.id
		sed -i '' "s/src-xxx-sync/$value/g" docker-sync.yml
		sed -i '' "s/src-xxx-sync/$value/g" docker-compose.yml
		n=$(grep --exclude=develop.sh -R "src-xxx-sync" * |wc -l)
		if [ $n -gt 0 ]; then
			printf "\e[38;5;1mThere are some problems with volume name yet. Operation failed\e[0m\n"
			exit 1;
		fi
	fi
}

updateSyncFile
if [ ! -f "docker-sync.id" ]; then
    printf "\e[38;5;1mFile docker-sync.id not found!\e[0m\n"
    exit 1
else
	SYNCID=`cat docker-sync.id`
fi
# Set environment variables for dev
export XDEBUG_HOST=$(ipconfig getifaddr en0) # Specific to Macintosh
export APP_ENV=${APP_ENV:-local}

if [ $# -gt 0 ]; then
	if [ $1 = "up" ]; then
		checkForNetwork
		if [ -f "docker-proxy.id" ]; then
			printf "Do you like to start proxy? (y/n)"
			read answer
			if [ $answer = "y" ]; then
				proxyup
			fi
		fi
		docker volume create --name=$SYNCID
		docker-compose "$@"
		docker-sync start
	fi
	if [ $1 = "down" ]; then
		docker-compose "$@"
		docker-sync stop
	fi
	if [ $1 = "remove" ]; then
		docker-compose "down"
		docker-sync stop
		docker rm $SYNCID
		docker volume rm $SYNCID
	fi
else
    docker-compose ps
fi