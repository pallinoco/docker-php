Pallino Docker basic package
===================
This package is built for development purpose, to reduce time to configure docker environment.
To create project in your host you type "composer create-project pallino/docker-php folder_name"

How to start and stop environment
---------------------------------
To start all containers you type Bin/docker.sh start from the main package directory, if you like to stop all containers type Bin/docker.sh stop.

What do you know before starting
--------------------------------
You find in this package a docker-compose.yml file with a basic docker configuration. Probably you should change this file in according with your needs, for example the port number of web and others services. The configuration is built using docker-compose and docker-sync, if you haven't yet installed docker or docker-sync you must installed them before proceeding.
You can find information how to install docker to [https://www.docker.com/ and about docker-sync] and docker-sync to [https://github.com/EugenMayer/docker-sync/wiki/1.-Installation].
If you like to user proxy before your container, so you can run multiple webserver in a different prot but you can access them using site name, you have to change the content of docker-proxy.id with path in your host to proxy main path.
All others features are completely managed by this package.

Configuration
-------------

###Virtualhosts###
In folder webserver/virtualhosts you find the virtualhost configuration for your web container, 000-default.conf is the configuration file for http virtualhost and default-ssl is for https. *Important*: you have to put into webserver/certificate_ssl the ssl certificates and change in the correct lines into default-ssl.conf the name of certificates you put. There must be 3 file: one is the key, one the certificate and one is the intermediate certificate.
 
###PHP Configuration###
You can change the behaviour of PHP changing php.ini or php-fpm.conf, you can find this file inside the folder webserver/php

###Database configuration###
What do you need about mysql starting feature you can find in docker-compose.yml, particularly is defined to use an environment file to create database and user, the file is environment.yml.
If you want to change some properties of MYSQL you can do it with the file my.cnf in mysql directory.
Inside the database folder there are the database physical files.
The mysql/logs is the directory where can be saved the mysql log file (you must configure it by my.cnf).

###Source directory###
This is the most important path, in it you must create a web folder where webserver links responding to web requests. 
This folder is mapped to docker volume created automatically during starting process, in this way docker-sync synchronize your src folder with /var/www/html folder in container machine. This process improve the performance than normal docker behaviour.
